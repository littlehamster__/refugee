﻿using System;
using UIKit;
using CoreGraphics;

namespace Hacktivist
{
	public class DocumentTableView : UIView
	{
		private DataPusher _pusher;
		public DocumentTableView (CGRect frame, string[] questions, DataPusher pusher) : base (frame)
		{
			_pusher = pusher;
			UITableView table = new UITableView (frame, UITableViewStyle.Plain);
			table.Source = new TableSource (questions, this);
			Add (table);
		}

		public void PushQuestion (string question) {
			_pusher.PushQuestion (question);
		}
	}

	public class TableSource : UITableViewSource {
		private string[] _questions;
		private DocumentTableView _mother;


		public TableSource(string[] questions, DocumentTableView mother) {
			_questions = questions;
			_mother = mother;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return _questions.Length;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override UITableViewCell GetCell (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (_questions[indexPath.Row]);

			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, _questions [indexPath.Row]);
				cell.TextLabel.Text = _questions [indexPath.Row];
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}


			return cell;

		}

		public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
		{
			tableView.CellAt (indexPath).Selected = false;
			_mother.PushQuestion(_questions[indexPath.Row]);
		}
	}
}

