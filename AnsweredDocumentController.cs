﻿using System;
using UIKit;
using CoreGraphics;

namespace Hacktivist
{
	public class AnsweredDocumentController : UIViewController
	{
		private UIButton _documentImage;
		private UILabel _documentTitle;
		private string _question = "";

		public AnsweredDocumentController ()
		{
			
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			nfloat bla = (nfloat) Math.Sqrt(2) * (UIScreen.MainScreen.Bounds.Width / 3);
			_documentImage = new UIButton(new CGRect(20, 86, UIScreen.MainScreen.Bounds.Width / 3, bla));
			_documentImage.SetImage(UIImage.FromFile ("hinweisblatt.jpg"), UIControlState.Normal);
			_documentImage.Layer.BorderColor = UIColor.Black.CGColor;
			_documentImage.Layer.BorderWidth = 1;
			Add (_documentImage);

			_documentTitle = new UILabel (new CGRect(40 + UIScreen.MainScreen.Bounds.Width / 3, 20, UIScreen.MainScreen.Bounds.Width - 60 - UIScreen.MainScreen.Bounds.Width / 3, (UIScreen.MainScreen.Bounds.Width / 3) * 2));
			_documentTitle.Lines = 5;
			_documentTitle.Text = _question;
			Add (_documentTitle);

			UIView seperator = new UIView (new CGRect (0, 86 + bla + 20, UIScreen.MainScreen.Bounds.Width, 1));
			seperator.BackgroundColor = UIColor.LightGray;
			Add (seperator);

			View.BackgroundColor = UIColor.White;
		}

		public void FillData (string data) {
			_question = data;
			if (_documentTitle != null) {
				_documentTitle.Text = data;
			}
			//_documentImage = UIIm
		}
	}
}

