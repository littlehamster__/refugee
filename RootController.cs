﻿using System;
using UIKit;

namespace Hacktivist
{
	public class RootController : UITabBarController
	{
		public RootController () : base ()
		{
			MyQuestionViewController myQuestions = new MyQuestionViewController ();
			NewQuestionViewController newQuestion = new NewQuestionViewController ();
			Feed feed = new Feed ();

			HacktivistController hack1 = new HacktivistController (myQuestions);
			//HacktivistController hack2 = new HacktivistController (newQuestion);
			HacktivistController hack3 = new HacktivistController (feed);

			myQuestions.Title = "My Questions";
			newQuestion.Title = "Ask Question";
			feed.Title = "Feed";

			UIViewController[] controller = { hack1, hack3 };
			SetViewControllers (controller, false);
		}
	}
}

