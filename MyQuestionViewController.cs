﻿using System;
using UIKit;

namespace Hacktivist
{
	public class MyQuestionViewController : UIViewController, DataPusher
	{

		private AnsweredDocumentController _answer;
		private NewQuestionViewController _questionController;

		public MyQuestionViewController () : base()
		{
			_questionController = new NewQuestionViewController();
			_answer = new AnsweredDocumentController();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			//this.Title = "My Questions";
			NavigationItem.Title = "My Questions";

			View.BackgroundColor = UIColor.White;
			string[] questions = { "Aufforderung zum ersten Interview", "Arbeitserlaubnis nach BGB 48a", "Ou Yue Immgigrant Nordkorea", "Zweites Interview nach ABC GHJ", "Unterlassung durch Blub von Kartoffel"};
			DocumentTableView table = new DocumentTableView(View.Frame, questions, this);
			View.AddSubview (table);
			//table.Center = new CoreGraphics.CGPoint (UIScreen.MainScreen.Bounds.Width / 2, UIScreen.MainScreen.Bounds.Height / 2);
			View.UserInteractionEnabled = true;

			UIBarButtonItem navItem = new UIBarButtonItem (UIBarButtonSystemItem.Camera, (object sender, EventArgs e) => {
				NavigationController.PresentModalViewController(_questionController, true);
			});

			NavigationItem.SetRightBarButtonItem (navItem, false);

		}



		#region DataPusher implementation
		public void PushQuestion (string data)
		{
			_answer.FillData(data);
			NavigationController.PushViewController (_answer, true);
		}
		#endregion


	}
}

